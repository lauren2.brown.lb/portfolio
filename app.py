
from flask import Flask, render_template

app = Flask(__name__)

file_path ='./content/dwp_digital.txt'

def read_txt_file(file_path):
    with open(file_path, 'r') as f:
        return f.read()

@app.route("/")
def home_page():
    text = read_txt_file('./content/about.txt')
    return render_template('home.html', text=text)

@app.route("/dwp_digital")
def dwp():
    script = read_txt_file('./content/dwp/dwp_digital.txt')
    script2 = read_txt_file('./content/dwp/team.txt')
    script3 = read_txt_file('./content/dwp/methods.txt')
    return render_template('dwp_digital.html', script=script, script2=script2, script3=script3)

@app.route("/about")
def about():
    text = read_txt_file('./content/about_me.txt')
    return render_template('about.html', text=text)

# @app.route("/projects")
# def projects():
#     return render_template('projects.html')
    
@app.route("/projects")
def one():
    script1 = read_txt_file('./content/project_1/situation.txt')
    script2 = read_txt_file('./content/project_1/task.txt')
    script3 = read_txt_file('./content/project_1/action/plan.txt')
    script4 = read_txt_file('./content/project_1/action/implementation.txt')
    script5 = read_txt_file('./content/project_1/action/automation.txt')
    script6 = read_txt_file('./content/project_1/action/problems.txt')
    script7 = read_txt_file('./content/project_1/result.txt')
    return render_template('project_one.html', script1=script1, script2=script2, script3=script3, script4=script4, script5=script5, script6=script6, script7=script7)

@app.route("/ksb")
def ksb():
    script1 = read_txt_file('./content/ksb/knowledge.txt')
    script2 = read_txt_file('./content/ksb/skills.txt')
    script3 = read_txt_file('./content/ksb/behaviours.txt')
    return render_template('ksb.html', script1=script1, script2=script2, script3=script3)

if __name__ == "__main__":
    app.run()