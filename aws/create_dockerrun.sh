cat <<EOF > Dockerrun.aws.json
{
  "AWSEBDockerrunVersion": "1",
  "Image": {
    "Name": "$CONTAINER_REGISTRY_URL:$CI_COMMIT_SHA",
    "Update": "true"
  },
  "Ports": [
    {
      "ContainerPort": "5000"
    }
  ]
}
EOF

echo "Created Dockerrun file"
cat Dockerrun.aws.json
