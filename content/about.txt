

I'm currently an Apprentice DevOps Engineer with DWP Digital after re-training from a Chemistry Lecturer.

I like problem solving and boardgames, so I'm a real nerd - and owning it! You can usually find me either destroying my house (we are renovating) or playing boardgames.

I'm currently learning about all things Terraform and Python, and haven't broken anything... yet!
