
B1: Exhibits enthusiasm, openness and an aptitude for working as part of a collaborative community, e.g. sharing best practice, pairing with team members, learning from others and engaging in peer review practices.

B2: Invests time and effort in their own development, recognising that technology evolves at a rapid rate.

B4: Is inclusive, professional and maintains a blameless culture. 