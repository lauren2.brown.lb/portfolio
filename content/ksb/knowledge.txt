
K3: How to use data ethically and the implications for wider society, with respect to the use of data, automation and artificial intelligence within the context of relevant data protection policy and legislation.

K6: A range of problem-solving techniques appropriate to the task at hand, such as affinity mapping, impact maps, plan-do-check-act/Deming.

K9: Different organisational cultures, the development frameworks utilised and how they can both complement each other and introduce constraints on delivery.

K18: Roles within a multidisciplinary team and the interfaces with other areas of an organisation.

K19: Different methods of communication and choosing the appropriate one - e.g. face-to-face (synchronous, high bandwidth), instant messaging, email (asynchronous, low bandwidth), visualisations vs. words.

K20: Pair/mob programming techniques and when to use each technique.

K22: How their occupation fits into the wider digital landscape and any current or future regulatory requirements.

K24: The difference between Software-as-a-Service (SaaS) v bespoke v enterprise tooling and how to make an informed choice that suits each use case.

K25: Maintain an awareness of cloud certification requirements.