
The ticket was assigned to me from the backlog, along with 5 others. This particular ticket was classed as a high priority task due to the monetary concerns around maintaining the high performance resources. The task itself was a relatively simple in that it only required the use of AWS services and Terraform. I was given this task due to the fact that I worked on a similar task previously involving both AWS and Terraform, and it was thought that this would allow me to build on top of the work I’d previously done. 

